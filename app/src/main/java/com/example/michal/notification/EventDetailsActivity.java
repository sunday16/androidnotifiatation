package com.example.michal.notification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.example.michal.notification.api.EventModel;

/**
 * Created by michal on 22.12.15.
 */
public class EventDetailsActivity extends AppCompatActivity {

    public static final String EVENT_EXTRA = "event";
    private static final String TAG = "EventDetailsActivity";

    public static Intent prepareIntent(EventModel eventModel, Context context) {
        Intent i = new Intent(context, EventDetailsActivity.class);
        i.putExtra(EVENT_EXTRA, eventModel);
        return i;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG,">>>>>>>>>>Finish :");
                EventDetailsActivity.this.finish();
            }
        });

        /*setSupportActionBar(toolbar);*/

        if (savedInstanceState == null) {
            Bundle bundle = getIntent().getExtras();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.content, EventDetailsFragment.newInstance(bundle))
                    .commit();

        }

    }
}
