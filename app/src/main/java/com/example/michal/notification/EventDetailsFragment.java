package com.example.michal.notification;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.michal.notification.api.EventModel;
import com.example.michal.notification.api.RetrofitAdapter;

/**
 * Created by michal on 22.12.15.
 */
public class EventDetailsFragment extends Fragment {

    private static final String TAG = "EventDetailsFragment";

    private TextView eventName;
    //private TextView eventShortDescr;
    private TextView eventLongDescr;
    private TextView eventOrganizer;
    private TextView eventDateEvent;
    private TextView eventCountry;
    private TextView eventState;
    private TextView eventCity;
    private TextView eventStreet;
    private TextView eventNumber;


    private EventModel event;


    public static EventDetailsFragment newInstance(Bundle args) {
        EventDetailsFragment fragment = new EventDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public EventDetailsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);


        if (savedInstanceState == null) {

            if (getArguments() != null) {
                event = (EventModel) getArguments().getSerializable(EventDetailsActivity.EVENT_EXTRA);
            }
        } else {
            event = (EventModel) savedInstanceState.getSerializable(EventDetailsActivity.EVENT_EXTRA);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //TODO
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_event_details, container, false);



        eventName = (TextView) view.findViewById(R.id.event_name);
        //eventShortDescr = (TextView) view.findViewById(R.id.event_short_descr);
        eventLongDescr = (TextView) view.findViewById(R.id.event_long_descr);
        eventOrganizer = (TextView) view.findViewById(R.id.event_organizer);
        eventDateEvent = (TextView) view.findViewById(R.id.event_date_event);
        eventCountry = (TextView) view.findViewById(R.id.event_country);
        eventState = (TextView) view.findViewById(R.id.event_state);
        eventCity = (TextView) view.findViewById(R.id.event_city);
        eventStreet = (TextView) view.findViewById(R.id.event_street);
        eventNumber = (TextView) view.findViewById(R.id.event_number);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.i(TAG, "Event: " + event);
        eventName.setText(event.getName());
        //eventShortDescr.setText(event.getShortDescr());
        eventLongDescr.setText(Html.fromHtml(event.getLongDescr()));
        eventDateEvent.setText(event.getDateEventToString());
        eventOrganizer.setText(event.getOrganizer());
        eventCountry.setText(event.getCountry());
        eventState.setText(event.getState());
        eventCity.setText(event.getCity());
        eventStreet.setText(event.getStreet());
        eventNumber.setText(event.getNumber());
        //
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable(EventDetailsActivity.EVENT_EXTRA, event);
    }


}
