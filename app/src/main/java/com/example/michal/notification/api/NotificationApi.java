package com.example.michal.notification.api;

import retrofit.Call;
import retrofit.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by michal on 21.12.15.
 */
public interface NotificationApi {

    @GET("/admin/events")
    Call<EventResponse> getEvents(@Query("page") int page, @Query("limit") int limit);

    @POST("/login")
    @FormUrlEncoded
    Call<UserResponse> login(@Field("username") String username,@Field("password")  String password);

    @POST("/login")
    @FormUrlEncoded
    Response<UserResponse> loginSync(@Field("username") String username, @Field("password")  String password);
}
