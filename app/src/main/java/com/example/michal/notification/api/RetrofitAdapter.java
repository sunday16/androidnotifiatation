package com.example.michal.notification.api;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.util.Base64;
import android.util.Log;

import com.example.michal.notification.EventDetailsActivity;
import com.example.michal.notification.EventDetailsFragment;
import com.example.michal.notification.MainActivity;
import com.example.michal.notification.NotificationPreferences;
/*import com.example.michal.notification.R;*/
import com.example.michal.notification.view.list.EventsListFragment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import okio.Buffer;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;

import retrofit.Retrofit;

/**
 * Created by michal on 21.12.15.
 */
public final class RetrofitAdapter {

    private static final String TAG = "RetrofitAdapter";

    private static RetrofitAdapter adapter;
    private NotificationApi notificationApi;
    private OkHttpClient okClient;
    private NotificationPreferences preferences;

    private RetrofitAdapter(final Context context) {

        preferences = NotificationPreferences.getInstance(context);

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

                    @Override
                    public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                        return new Date(json.getAsLong());
                    }
                })
                .create();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        okClient = new OkHttpClient();

        Interceptor inc = new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {

                Request original = chain.request();

                String sessionCookie = preferences.getSessionCookie();


                if (sessionCookie != null && !sessionCookie.isEmpty()) {

                    Request.Builder requestBuilder = original.newBuilder()
                            .addHeader("Cookie", "JSESSIONID" + "=" + sessionCookie);

                    Request request = requestBuilder.build();
                    Response response = chain.proceed(request);


                    if (response.code() == 401) {

                        final String username = preferences.getLogin();
                        final String pass = preferences.getPass();

                        retrofit.Response<UserResponse> res = login(username, pass).execute();
                        NotificationPreferences newPreferences = NotificationPreferences.getInstance(context);

                        Headers cookies = res.headers();

                        String newCookie = cookies.get("Set-Cookie");

                        int first = newCookie.indexOf("=");
                        int end = newCookie.indexOf(";");

                        String newC = newCookie.substring(first + 1, end);
                        newPreferences.saveSessionInfo(newC, username, pass);

                        Request newRequest = original.newBuilder()
                                .removeHeader("Cookie")
                                .addHeader("Cookie", "JSESSIONID" + "=" + newPreferences.getSessionCookie())
                                .build();


                        Response proceed = chain.proceed(newRequest);

                        return proceed;

                    } else {
                        return response;
                    }
                }

                Response proceed = chain.proceed(original);
                return proceed;

            }
        };

        okClient.interceptors().add(inc);
        okClient.interceptors().add(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://52.17.77.147")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okClient)
                .build();

        notificationApi = retrofit.create(NotificationApi.class);


    }


    public static RetrofitAdapter getInstance(Context context) {
        if (adapter == null) return new RetrofitAdapter(context);
        return adapter;
    }

    public Call<UserResponse> login(String username, String password) {
        return notificationApi.login(username, password);
    }

    public Call<EventResponse> getEvents(int page, int limit) {
        return notificationApi.getEvents(page, limit);
    }


}
