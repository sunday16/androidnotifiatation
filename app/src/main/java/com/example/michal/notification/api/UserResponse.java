package com.example.michal.notification.api;

import java.util.List;

/**
 * Created by michal on 08.01.16.
 */
public class UserResponse {

    private String isSuccess;
    private long userId;
    private String username;


    public UserResponse(String isSuccess, long userId, String username) {
        this.isSuccess = isSuccess;
        this.userId = userId;
        this.username = username;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "UserResponse{" +
                ", isSuccess='" + isSuccess + '\'' +
                ", userId=" + userId +
                ", username='" + username + '\'' +
                '}';
    }
}
