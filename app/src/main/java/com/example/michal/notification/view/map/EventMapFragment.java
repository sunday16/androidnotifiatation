package com.example.michal.notification.view.map;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.michal.notification.EventDetailsActivity;
import com.example.michal.notification.R;
import com.example.michal.notification.api.EventModel;
import com.example.michal.notification.api.EventResponse;
import com.example.michal.notification.api.RetrofitAdapter;
import com.example.michal.notification.network.NetworkManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by michal on 21.12.15.
 */
public class EventMapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener {

    private static final String TAG = "MapFragment";
    private static final LatLng MAP_CENTER = new LatLng(52.246023, 20.585003);

    private GoogleMap map;
    private Map<String, EventModel> markerEvents;
    private View noInternetInfo;
    private View retryButton;
    private ProgressBar retryProgress;
    private boolean checkNetworrkState = false;
    private SupportMapFragment mapFragment;

    public static EventMapFragment newInstance() {
        return new EventMapFragment();
    }

    public EventMapFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_map, container, false);
        noInternetInfo = view.findViewById(R.id.no_internet_map);
        retryButton = view.findViewById(R.id.retry_button_map);
        retryProgress = (ProgressBar) view.findViewById(R.id.progres_retry_map);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (!NetworkManager.isConnected(getActivity())) {
            //noinspection ConstantConditions
            mapFragment.getView().setVisibility(View.INVISIBLE);
            noInternetInfo.setVisibility(View.VISIBLE);
            retryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    retryButton.setEnabled(false);

                    Log.i(TAG, "on click " + checkNetworrkState);

                    if (!checkNetworrkState) {
                        Log.i(TAG, "check network state");
                        checkNetworrkState = true;
                        retryProgress.setVisibility(View.VISIBLE);
                        retryButton.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (isAdded()) {
                                    if (NetworkManager.isConnected(getActivity())) {
                                        Log.i(TAG, "Have internet connection");
                                        showMap();
                                        retryProgress.setVisibility(View.GONE);
                                    }
                                    retryButton.setEnabled(true);
                                    checkNetworrkState = false;
                                    retryProgress.setVisibility(View.GONE);
                                }
                            }
                        }, 4000);
                    }
                }
            });
        } else {
            showMap();
        }
    }

    private void showMap() {
        retryButton.setVisibility(View.GONE);
        noInternetInfo.setVisibility(View.GONE);
        mapFragment.getView().setVisibility(View.VISIBLE);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;
        map.setOnMarkerClickListener(this);
        map.setOnInfoWindowClickListener(this);

        int px = getResources().getDimensionPixelSize(R.dimen.map_dot_marker_size);
        final Bitmap mDotMarkerBitmap = Bitmap.createBitmap(px, px, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mDotMarkerBitmap);
        Drawable shape = getResources().getDrawable(R.drawable.map_dot);
        shape.setBounds(0, 0, mDotMarkerBitmap.getWidth(), mDotMarkerBitmap.getHeight());
        shape.draw(canvas);

        RetrofitAdapter retrofitAdapter = RetrofitAdapter.getInstance(getActivity());

        retrofitAdapter.getEvents(1, 40).enqueue(new Callback<EventResponse>() {
            @Override
            public void onResponse(Response<EventResponse> response, Retrofit retrofit) {
                if (isAdded()) {
                    if (response.isSuccess()) {
                        List<EventModel> allResult = response.body().getAllResult();
                        markerEvents = new HashMap<>(allResult.size());

                        for (EventModel event : allResult) {
                            LatLng eventPosition = new LatLng(event.getLat(), event.getLon());

                            MarkerOptions markerOption = new MarkerOptions()
                                    .icon(BitmapDescriptorFactory.fromBitmap(mDotMarkerBitmap))
                                    .position(eventPosition)
                                    .title(event.getName())
                                    .snippet(event.getShortDescr());
                            Marker marker = map.addMarker(markerOption);

                            markerEvents.put(marker.getId(), event);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d(TAG, "ERROR Throwable: " + t);
            }
        });

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(MAP_CENTER)
                .zoom(5)
                .build();

        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        marker.showInfoWindow();
        return true;
    }

    private void startEventDetails(EventModel event) {
        Intent i = EventDetailsActivity.prepareIntent(event, getContext());
        startActivity(i);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        String markerId = marker.getId();
        EventModel event = markerEvents.get(markerId);

        if (event != null) {
            startEventDetails(event);
        }
    }
}
