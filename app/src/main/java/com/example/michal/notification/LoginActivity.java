package com.example.michal.notification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by michal on 10.01.16.
 */
public class LoginActivity extends AppCompatActivity {

    public static Intent prepareIntent(Context context) {
        Intent i = new Intent(context, LoginActivity.class);
        return i;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.content, LoginFragment.newInstance())
                    .commit();
        }
    }
}
