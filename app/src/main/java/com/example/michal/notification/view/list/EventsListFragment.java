package com.example.michal.notification.view.list;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.michal.notification.EventDetailsActivity;
import com.example.michal.notification.R;
import com.example.michal.notification.api.EventModel;
import com.example.michal.notification.api.EventResponse;
import com.example.michal.notification.api.RetrofitAdapter;
import com.example.michal.notification.api.UserResponse;
import com.example.michal.notification.network.NetworkManager;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by michal on 21.12.15.
 */
public class EventsListFragment extends Fragment implements EventAdapter.OnItemClickListener {

    private static final String TAG = "EventList";
    private static final int PAGE_SIZE = 6;

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private ProgressBar progressBar;
    private EventAdapter eventAdapter;
    private TextView errorInfo;
    private View noInternetInfo;
    private View retryButton;
    private ProgressBar retryProgress;
    private boolean checkNetworrkState = false;

    RetrofitAdapter retrofit;

    private boolean isLastPage = false;
    private boolean isLoading = false;
    private int currentPage = 1;


    public static EventsListFragment newInstance() {

        return new EventsListFragment();
    }

    public EventsListFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        retrofit = RetrofitAdapter.getInstance(getActivity());

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_events_list, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        progressBar = (ProgressBar) view.findViewById(R.id.progres_bar);
        errorInfo = (TextView) view.findViewById(R.id.error_info);
        noInternetInfo = view.findViewById(R.id.no_internet);
        retryButton = view.findViewById(R.id.retry_button);
        retryProgress = (ProgressBar) view.findViewById(R.id.progres_retry);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        eventAdapter = new EventAdapter();
        eventAdapter.setClickListener(this);
        recyclerView.setAdapter(eventAdapter);
        recyclerView.addOnScrollListener(recyclerViewScrollListener);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retryButton.setEnabled(false);

                Log.i(TAG, "on click " + checkNetworrkState);

                if (!checkNetworrkState) {
                    Log.i(TAG, "check network state");
                    checkNetworrkState = true;
                    retryProgress.setVisibility(View.VISIBLE);
                    retryButton.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (NetworkManager.isConnected(getActivity())) {

                                showEvents();
                                retryProgress.setVisibility(View.GONE);
                            }
                            retryButton.setEnabled(true);
                            checkNetworrkState = false;
                            retryProgress.setVisibility(View.GONE);
                        }
                    }, 4000);
                }
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!NetworkManager.isConnected(getActivity())) {
            Log.i(TAG, ">>>>>Is not connected");
            progressBar.setVisibility(View.GONE);
            noInternetInfo.setVisibility(View.VISIBLE);
            AlertDialog.Builder dialog = buildDialog(getActivity());
            dialog.show();
        } else {
            showEvents();
        }

    }

    private void showEvents() {
        noInternetInfo.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        retrofit.getEvents(currentPage, PAGE_SIZE).enqueue(findEventsFirstFetchCallback);
    }

    private AlertDialog.Builder buildDialog(Context c) {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle(R.string.no_internet_title);
        builder.setMessage(R.string.no_internet_info);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });

        return builder;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        currentPage = 1;
        eventAdapter.setClickListener(null);
        removeListeners();
    }

    private void removeListeners() {
        recyclerView.removeOnScrollListener(recyclerViewScrollListener);
    }

    private OnScrollListener recyclerViewScrollListener = new RecyclerView.OnScrollListener() {

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = linearLayoutManager.getChildCount();
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

            if (!isLoading && !isLastPage) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= PAGE_SIZE) {
                    loadMoreItems();
                }
            }
        }
    };

    private Callback<EventResponse> findEventsFirstFetchCallback = new Callback<EventResponse>() {
        @Override
        public void onResponse(Response<EventResponse> response, Retrofit retrofit) {
            Log.d(TAG, "First fetch succes response: " + response.body());

            if (isAdded()) {
                progressBar.setVisibility(View.GONE);
                isLoading = false;

                if (response.isSuccess()) {
                    EventResponse eventResponse = response.body();
                    if (eventResponse != null) {
                        List<EventModel> events = eventResponse.getAllResult();
                        if (events != null) {
                            eventAdapter.addAll(events);

                            if (events.size() >= PAGE_SIZE) {
                                eventAdapter.addLoading();
                            } else {
                                isLastPage = true;
                            }
                        }
                    }
                } else {
                    int code = response.code();
                    switch (code) {
                        case 500:
                            errorInfo.setText("Server not response");
                            break;
                        case 404:
                            errorInfo.setText("Server not available");
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        @Override
        public void onFailure(Throwable t) {
            Log.d(TAG, "failure response: " + t);
            if (isAdded()) {
                progressBar.setVisibility(View.GONE);
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        }

    };


    private Callback<EventResponse> findEventsNextFetchCallback = new Callback<EventResponse>() {
        @Override
        public void onResponse(Response<EventResponse> response, Retrofit retrofit) {
            if (isAdded()) {

                eventAdapter.removeLoading();
                isLoading = false;
                if (response.isSuccess()) {
                    EventResponse eventResponseBody = response.body();
                    if (eventResponseBody != null) {
                        List<EventModel> events = eventResponseBody.getAllResult();
                        if (events != null) {
                            eventAdapter.addAll(events);

                            if (events.size() >= PAGE_SIZE) {
                                eventAdapter.addLoading();
                            } else {
                                isLastPage = true;
                            }
                        }
                    }
                }
            }
        }

        @Override
        public void onFailure(Throwable t) {
            if (isAdded()) {
                progressBar.setVisibility(View.GONE);
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }

        }
    };


    private void loadMoreItems() {
        isLoading = true;
        currentPage += 1;
        retrofit.getEvents(currentPage, PAGE_SIZE).enqueue(findEventsNextFetchCallback);
    }

    @Override
    public void onItemClick(int position, View view) {
        EventModel event = eventAdapter.getEvent(position);
        startEventDetails(event);
    }

    private void startEventDetails(EventModel event) {
        Intent i = EventDetailsActivity.prepareIntent(event, getContext());
        startActivity(i);
    }
}
