package com.example.michal.notification;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.michal.notification.api.NotificationApi;

/**
 * Created by michal on 10.01.16.
 */
public final class NotificationPreferences {

    private static final String SESSION_COOKIE = "Cookie";
    private static final String LOGIN = "Login";
    private static final String PASS = "Pass";


    public static NotificationPreferences notificationPreferences;
    public SharedPreferences sharedPreferences;

    public NotificationPreferences(Context context) {
        sharedPreferences = context.getSharedPreferences(
                "notification", Context.MODE_PRIVATE);

    }

    public static NotificationPreferences getInstance(Context context) {
        if (notificationPreferences == null) {
            notificationPreferences = new NotificationPreferences(context);
        }
        return notificationPreferences;
    }

    public String getSessionCookie() {
        return sharedPreferences.getString(SESSION_COOKIE, "");
    }

    public String getLogin() {
        return sharedPreferences.getString(LOGIN, "");
    }

    public String getPass() {
        return sharedPreferences.getString(PASS, "");
    }

    public void saveSessionInfo(String cookieSession, String login, String pass) {
        sharedPreferences.edit().putString(SESSION_COOKIE, cookieSession)
                .putString(LOGIN, login)
                .putString(PASS, pass)
                .apply();
    }

}
