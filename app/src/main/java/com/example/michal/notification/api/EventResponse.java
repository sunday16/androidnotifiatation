package com.example.michal.notification.api;

import java.util.List;

/**
 * Created by michal on 21.12.15.
 */
public class EventResponse {

    public long total;
    public List<EventModel> allResult;

    public EventResponse() {
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<EventModel> getAllResult() {
        return allResult;
    }

    public void setAllResult(List<EventModel> allResult) {
        this.allResult = allResult;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        if (allResult != null && !allResult.isEmpty()) {
            for (EventModel e : allResult)
                sb.append(e).append("\n");
        } else {
            sb.append("no events");
        }

        return "EventResponse{" +
                "total=" + total +
                ", allResult=" + sb.toString() +
                '}';
    }
}
