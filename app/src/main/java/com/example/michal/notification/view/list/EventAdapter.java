package com.example.michal.notification.view.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.michal.notification.R;
import com.example.michal.notification.api.EventModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by michal on 22.12.15.
 */
public class EventAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

    public interface OnItemClickListener {

        void onItemClick(int position, View view);
    }

    public static final int ITEM = 0;
    public static final int LOADING = 1;

    private List<EventModel> events;
    public OnItemClickListener clickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(int position, View view) {

        }
    };
    private boolean isLoadingFooterAdded = false;

    public EventAdapter() {
        this.events = new ArrayList<>();
    }

    public void setClickListener(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case ITEM:
                viewHolder = createEventViewHolder(parent);
                break;
            case LOADING:
                viewHolder = createLoadingViewHolder(parent);
                break;
            default:
                throw new IllegalStateException("Unknow type with number : " + viewType);
        }

        return viewHolder;
    }

    private RecyclerView.ViewHolder createLoadingViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_layout, parent, false);
        return new MoreViewHolder(view);
    }

    private RecyclerView.ViewHolder createEventViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_list_item, null);
        EventViewHolder viewHolder = new EventViewHolder(view, clickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case ITEM:
                onBindEventView((EventViewHolder) holder, position);
                break;
            case LOADING:
                onBindLoadingViewHolder((MoreViewHolder) holder);
                break;
        }
    }

    private void onBindEventView(EventViewHolder holder, int position) {
        EventModel event = events.get(position);
        holder.eventName.setText(event.getName());
        holder.eventShortDescr.setText(event.getShortDescr());
        holder.eventDate.setText(sdf.format(event.getDateEvent()));
    }

    private void onBindLoadingViewHolder(MoreViewHolder holder) {

    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == events.size() - 1 && isLoadingFooterAdded) ? LOADING : ITEM;
    }

    public EventModel getEvent(int position) {
        return events.get(position);
    }


    public void addLoading() {
        isLoadingFooterAdded = true;
        add(new EventModel());
    }

    public void removeLoading() {
        isLoadingFooterAdded = false;
        int position = events.size() - 1;
        EventModel eventModel = events.get(position);

        if (eventModel != null) {
            events.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void add(EventModel eventModel) {
        events.add(eventModel);
        notifyItemInserted(events.size() - 1);
    }

    public void addAll(List<EventModel> events) {
        for (EventModel e : events) {
            add(e);
        }
    }

    public static class EventViewHolder extends RecyclerView.ViewHolder {

        private OnItemClickListener clickListener;

        TextView eventName;
        TextView eventDate;
        TextView eventShortDescr;

        public EventViewHolder(View itemView, OnItemClickListener clickListener) {
            super(itemView);
            this.clickListener = clickListener;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventViewHolder.this.clickListener.onItemClick(getAdapterPosition(), v);
                }
            });
            eventName = (TextView) itemView.findViewById(R.id.event_name);
            eventDate = (TextView) itemView.findViewById(R.id.event_date);
            eventShortDescr = (TextView) itemView.findViewById(R.id.event_description);
        }
    }

    public static class MoreViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBarMore;

        public MoreViewHolder(View itemView) {
            super(itemView);
            progressBarMore = (ProgressBar) itemView.findViewById(R.id.progres_bar_more);
        }
    }
}
