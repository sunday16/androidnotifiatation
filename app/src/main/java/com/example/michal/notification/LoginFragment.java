package com.example.michal.notification;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.EditText;
import android.widget.TextView;

import com.example.michal.notification.api.RetrofitAdapter;
import com.example.michal.notification.api.UserResponse;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by michal on 10.01.16.
 */
public class LoginFragment extends BaseFragment {

    private static final String TAG = "LoginFragment";

    @Bind(R.id.username)
    EditText username;

    @Bind(R.id.password)
    EditText password;

    @Bind(R.id.login_err)
    TextView loginError;

    private RetrofitAdapter retrofitAdapter;
    private NotificationPreferences preferences;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        retrofitAdapter = RetrofitAdapter.getInstance(this.getContext());
        preferences = NotificationPreferences.getInstance(this.getContext());
    }

    @Override
    public int fragmentLayout() {
        return R.layout.login_fragment;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        loginError.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);




        //retrofitAdapter.login()
    }

    @OnClick(R.id.submit)
    public void onSubmitClick(View view) {

        Log.i(TAG, "on submit click");


        final String login = username.getText().toString();
        final String pass = password.getText().toString();

        retrofitAdapter.login(login, pass).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Response<UserResponse> response, Retrofit retrofit) {
                Log.i(TAG, response.toString());

                if (response.isSuccess()) {
                    loginError.setVisibility(View.INVISIBLE);
                    Log.i(TAG, "Success : " + response.body().toString());


                    Headers cookies = response.headers();

                    String sessionCookie = cookies.get("Set-Cookie");

                    int first = sessionCookie.indexOf("=");
                    int end = sessionCookie.indexOf(";");

                    String cookieSession = sessionCookie.substring(first + 1, end);
                    preferences.saveSessionInfo(cookieSession, login, pass);

                    Map<String, List<String>> stringListMap = cookies.toMultimap();

                    for (Map.Entry<String, List<String>> e : stringListMap.entrySet()) {

                        Log.i(TAG, "KEY: " + e.getKey() + " , VALUE " + Arrays.toString(e.getValue().toArray()));
                    }


                    startMainActivity();

                } else {
                    showFailure();

                    ResponseBody error = response.errorBody();
                    try {
                        Log.i(TAG, error.string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    String message = response.message();
                    Log.i(TAG, message);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.i(TAG, "Failure : ");
                showFailure();
            }
        });

    }


    private void showFailure() {
        Log.i(TAG, "SHOW Failure : ");

        if (!isAdded()) {
            return;
        }
        AnimationSet set = new AnimationSet(true);

        AlphaAnimation start = new AlphaAnimation(0, 1);
        start.setDuration(400);
        AlphaAnimation end = new AlphaAnimation(1, 0);
        end.setDuration(400);
        end.setStartOffset(3400);
        end.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(isAdded()){
                    loginError.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        set.addAnimation(start);
        set.addAnimation(end);

        loginError.setVisibility(View.VISIBLE);
        loginError.startAnimation(set);
    }

    private void startMainActivity() {
        Intent i = MainActivity.prepareIntent(getContext());
        Log.i(TAG, "START MAIN ACTIVITY LOGIN: ");
        startActivity(i);
        getActivity().finish();
    }
}
